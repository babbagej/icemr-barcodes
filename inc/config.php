<?php

define('PROJECTS', implode(',' ,array(120, 121, 124, 125)));

//Initializes the $redcap_version variable to the value set in the redcap_config table
require_once dirname(__FILE__) . '/../../../redcap_connect.php';

define('BARCODE_REDCAP_VERSION_PATH', dirname(__FILE__) . '/../../../redcap_v' . $redcap_version . '/');

define('BARCODE_INCLUDE_PATH', dirname(__FILE__) . '/..');

require_once BARCODE_REDCAP_VERSION_PATH . 'Config/init_project.php';

require_once BARCODE_INCLUDE_PATH . '/inc/functions.php';

define('BARCODE_PATH', APP_PATH_WEBROOT . '../plugins/barcodes/');
define('BARCODE_PATH_JS', BARCODE_PATH . 'js/');
define('BARCODE_PATH_CSS', BARCODE_PATH . 'css/');
define('BARCODE_PATH_INC', BARCODE_PATH . 'inc/');

define('PID', $project_id);

cleanPost();
allowProjects(explode(',', PROJECTS));


?>