<?php



function getIdField($pid) {
	return REDCap::getRecordIdField();

}


function getEvents($project_id) {
    // Check if project is longitdudinal first
    if (!REDCap::isLongitudinal()) {
    
    /*****************
    
    THIS IS A HACK.  The REDCap object does not return the eventID for non-longitudinal studies.  This is used as a unique proxy for that id
    ******************/
     return array(0=>(-1*$project_id));         

    }

    // Print out the names of all events in the project (append the arm name if multiple arms exist)
    $events = REDCap::getEventNames(true, false);
    $eventsNames = REDCap::getEventNames(false, false);
    foreach ($events as $id => $unique_event_name) {
        $event_array[$id]['unique_name'] = $unique_event_name;
         $event_array[$id]['name'] = $eventsNames[$id];

        }
	return $event_array;
}


function cleanPost() {
    foreach ($_POST as $key=>$value) {
		//Remove any backslashes or single- or double-quotes
		$orig = array("'","\"","\\");
		$repl = array("","","");
		$_POST[$key] = str_replace($orig,$repl,$value);
	}
}

function confirmSamples($pid, $record) {
    $sql = "select es.sample_type_id, es.event_id from plugin_barcodes_events_samples es 
    join plugin_barcodes_sample_types st on es.sample_type_id= st.sample_type_id
     left join plugin_barcodes_samples s on es.event_id = s.event_id and es.sample_type_id = s.sample_type_id and s.record = {$record}
     where st.project_id = {$pid} and s.record is null";
  
     $q = query($sql);
	
    $samplesToGenerate = array();
    
    if (mysqli_num_rows($q) != 0) { //there are event sample types that exist, but are not defined for this subject
        while ( $row = mysqli_fetch_assoc($q)) 
        {
	          $samplesToGenerate[] = $row;
           
	      }
        
        foreach($samplesToGenerate as $sample) {
             generateEventSample($pid, $record, $sample['sample_type_id'], $sample['event_id']);  
        }
    }
	
}

function generateEventSample($pid, $record, $sample_type_id, $event_id) {
    error_log("{$record} {$sample_type_id}, {$event_id}" ); 
    $sql = "insert into plugin_barcodes_samples (project_id, record, sample_type_id, event_id) values ({$pid}, {$record},{$sample_type_id}, {$event_id})";
     $q = query($sql);
    
}

function query($sql) {
  global $conn;
  return mysqli_query($conn, $sql);
}

?>