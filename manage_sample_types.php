<?php

require_once dirname(__FILE__) . '/inc/config.php';

//require_once dirname(__FILE__) . '/inc/functions.php';
/*
require_once dirname(__FILE__) . '/../../redcap_connect.php';

require_once 'Config/init_project.php';
include APP_PATH_DOCROOT . 'ProjectGeneral/header.php';
include APP_PATH_DOCROOT . "ProjectSetup/tabs.php";
*/

include APP_PATH_DOCROOT . 'ProjectGeneral/header.php';

if (!empty($_POST)) 	
{
    $event_array = getEvents($project_id);

	if(isset($_POST['sample_type_id'])) {
	
	
	//var_dump($event_array);
	foreach($event_array as $event_id => $event) {
		if(isset($_POST['event'][$event_id])) {
			$sql = "insert ignore into plugin_barcodes_events_samples (sample_type_id, event_id) values (".$_POST['sample_type_id'].", $event_id)";
			
			$q = query($sql);
		
		}
		else {
			$sql = "delete from plugin_barcodes_events_samples where event_id = $event_id and sample_type_id = ".$_POST['sample_type_id'];
			//echo $sql;
		    $q = query($sql);
		}
	
	}
	//exit;

	
	//Update this field in redcap_projects
	$sql = "update plugin_barcodes_sample_types set name = '".$_POST['__TITLE__']."' where sample_type_id = ".$_POST['sample_type_id'];
	
        if (query($sql)) 
        {
            // Log the data change
            REDCap::logEvent("Updated sample types", null, $sql);
        }
        
	}
	else {
		$sql = "insert into plugin_barcodes_sample_types (name, project_id) values ('".$_POST['__TITLE__']."', $project_id)";
        $q = query($sql);
        if ($q) 
        {
    // Log the data change
            $sample_id = mysqli_insert_id($conn);
            REDCap::logEvent("Updated sample types", null, $sql);
            
            if (!REDCap::isLongitudinal()) {
                $sql = "insert ignore into plugin_barcodes_events_samples (sample_type_id, event_id) values (".$sample_id.", $event_array[0])";
                //echo $sql;
                //exit;
                $q = query($sql); 
            }
            
        }
        
	}
	//$q = mysql_query($sql);
	
    
    			
	//After adding/editing this report, refresh page to display it.
	//exit;
	redirect($_SERVER['PHP_SELF']."?pid=$project_id&newquery=1");
	exit;
	
}
#########################################################################################################





#########################################################################################################
//Delete this query if user clicked "delete"
if (isset($_GET['delete'])) {
	
	$sample_type_id = $_GET['sample_type_id'];
	
	$sql = "delete from plugin_barcodes_sample_types where sample_type_id = $sample_type_id";
	$q = query($sql);
		
	// Logging
	if ($q) log_event($sql,"redcap_projects","MANAGE",$project_id,"project_id = $project_id","Delete sample");
	
    $sql = "delete from plugin_barcodes_events_samples where sample_type_id = $sample_type_id";
	$q = query($sql);
    
	//After deleting this report, refresh page
	redirect($_SERVER['PHP_SELF']."?pid=$project_id&deleted_query=1");
	exit;
	
}


//Edit Query set up

if (isset($_GET['sample_type_id'])) {
	
	if (!is_numeric($_GET['sample_type_id'])) {
		redirect(APP_PATH_WEBROOT . "index.php?pid=" . PROJECT_ID);
		exit;
	}
	
	$sample_type_id = $_GET['sample_type_id'];
	//$this_query_array = $query_array[$query_id]; //$query_array originates from an eval of $report_builder in config.php

}
	

// Header
include APP_PATH_DOCROOT . 'top.php';

renderPageTitle("<img src='".APP_PATH_IMAGES."wrench.png'> Manage Sample Types");

	
	
#########################################################################################################	
//Show all existing queries and their titles and allow user to create a new query

//Give confirmation of adding new query
if (isset($_GET['newquery'])) {
	print "<div align=center style='max-width:700px;'><span class='darkgreen'><img src='".APP_PATH_IMAGES."tick.png' 
		class='imgfix'>Your sample has been saved</span></div><br>";
} elseif (isset($_GET['deleted_query'])) {
	print "<div align=center style='max-width:700px;'><span class='red'><img src='".APP_PATH_IMAGES."exclamation.png' 
		class='imgfix'>Your sample has been deleted</span></div><br>";
} 

print "<p>You may use this page to define the types of barcodes needed for a particular study.  You may also set some details
about the sample such as whether it is a single sample per subject or multiple.<br>";
	
print "<form method='post' action='".$_SERVER['PHP_SELF']."?pid=$project_id' target='_self' 
enctype='multipart/form-data' name='form'> ";	
	
if (isset($_GET['sample_type_id'])) {

	print '<br><div id="sub-nav" style="max-width:700px;"><ul>';
	print '<li class="active"><a style="font-size:14px;color:#393733" href="javascript:;">Edit Existing Sample</a></li>';
	print '</ul></div>';
	$q = query("select * from plugin_barcodes_sample_types where sample_type_id = {$sample_type_id}");
	$editSampleType = mysqli_fetch_assoc($q);
	renderEventProfile($_GET['pid'], $editSampleType);
	//var_dump($editSampleType);
	//print "<p>instructions</p><br>";

} else {
    $q = query("select * from plugin_barcodes_sample_types where project_id = {$_GET['pid']}");
	// If project doesn't exist, send to Home page
//	if (mysql_num_rows($q) < 1) redirectHome();
	// Assign all redcap_projects table fields as variables and/or constants
	
	while ( $row = mysqli_fetch_assoc($q)) 
	{
	   // var_dump($row);
		$sample_array[$row['sample_type_id']] = $row;
		//$sample[$value['sample_type_id']]['name'] = $value['name'];	
		//$sample[$value['sample_type_id']]['multi'] = $value['n'];
	}

	//My Reports list
	if (!empty($sample_array)) 
	{
		print "<div style='max-width:700px;'><table width=100% cellpadding=3 cellspacing=0 style='border:1px solid #D0D0D0;font-family:Verdana,Arial;'>
				<tr><td style='border:1px solid #AAAAAA;font-size:14px;font-weight:bold;padding:5px;text-align:left;background-color:#DDDDDD;' colspan='3'>
					My Samples
				</td></tr>";	
		
        $i = 1;
        foreach ($sample_array as $key => $this_array) {
    //	var_dump($this_array);
            $thisbg = ($i%2) == 0 ? '#FFFFFF' : '#EEEEEE';
            $this_menu_item = $this_array['name'];
        

            print "<tr style='background-color: $thisbg;'>
                <td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:right;width:30px;'>$i.)&nbsp;</td>
                <td class='notranslate' style='padding: 3px 0 3px 0;font-size:11px;font-weight:bold;'>$this_menu_item</td>
                <td style='padding: 3px 0 3px 10px;text-align:right;'>
                    <span style='color:#C0C0C0;'>
                    [<a style='color:#202020;font-size:11px;' href='" . BARCODE_PATH . basename(__FILE__)."?pid=$project_id&sample_type_id=$key'>edit</a>]
                    [<a style='color:#800000;font-size:11px;' href='javascript:;' 
                        onclick=\"if(confirm('Are you sure you want to delete this sample type?    \\n'))  
                        window.location.href='" . BARCODE_PATH . basename(__FILE__)."?pid=$project_id&sample_type_id=$key&delete=1'+addGoogTrans(); return false;\">{$lang['report_builder_13']}</a>]
                    </span>
                </td>
                </tr>";
            $i++;

		}
		print "</table></div>";
	}
	print "<br>";

	
	renderEventProfile($_GET['pid']);
	//Create a New Report
	print '<br><br><div id="sub-nav" style="margin:0px;max-width:700px;"><ul>';
	print '<li class="active"><a style="font-size:14px;color:#393733" href="javascript:;">Create a New Sample</a></li>';
	print '</ul></div><br><br><br>';
	//print "<p>instructions</p><br>";
}





if(isset($editSampleType)) {
	print "<input type = 'hidden' name = 'sample_type_id' value = $sample_type_id>";
	$name = $editSampleType['name'];
	$multi = $editSampleType['multi'] ? 'selected' : '';
	$single = $editSampleType['multi'] ? '' : 'selected';
	}
else {
	$single = 'selected';
}	

print  "<div id='query_table' style='max-width:700px;'>
		<table class='form_border' width=100%>
		<tr>
			<td class='header' style='color:#800000;width:120px;height:50px;'>
				Sample Type
			</td>
			<td class='header'  style='height:50px;'>
				<input type='text' id='visible-TITLE' value='$name' size=60 maxlength=60 style='font-weight:bold;'
					onkeydown='if(event.keyCode == 13) return false;' 
					onchange=\"document.getElementById('__TITLE__').value=document.getElementById('visible-TITLE').value;\">
				<input type='hidden' name='__TITLE__' id='__TITLE__' value='$name'>
			</td>
		</tr>
		
		
		";
		

	

		
//Set up submit/cancel buttons	
$save_button_text = 'Save Sample';
$cancel_button = "";	
if (isset($_GET['query_id'])) {
	$save_button_text = 'Save Changes';
	$cancel_button = "<input type='button' value='Cancel' onclick=\"window.location.href='".APP_PATH_WEBROOT."barcode_builder.php?pid=$project_id'+addGoogTrans();\">";
}

//Bottom of table with Ordering and Submit button		
print "<div style='max-width:700px;'>
	<table class='form_border' width=100%>
						
		
	</table>
	</div>
	<br>
	<div align='center' style='max-width:700px;'>		
		<div>
			<input type='submit' name='submit-button' value='$save_button_text' onclick=\"if(document.getElementById('visible-TITLE').value.length==0){
				alertbad(document.getElementById('visible-TITLE'),'{$lang['report_builder_24']}');return false;}\"> 
			$cancel_button
		</div>
	</div>
	</form>";

include APP_PATH_DOCROOT . 'ProjectGeneral/footer.php';




function renderEventProfile($pid, $edit_sample_id = null){
$q = query("select * from plugin_barcodes_events_samples es join plugin_barcodes_sample_types st 
on es.sample_type_id= st.sample_type_id where project_id = $pid");
	// If project doesn't exist, send to Home page
//	if (mysql_num_rows($q) < 1) redirectHome();
	// Assign all redcap_projects table fields as variables and/or constants
	
	while ( $row = mysqli_fetch_assoc($q)) 
	{
	   // var_dump($row);
		$event_sample_array[$row['sample_type_id']][$row['event_id']] = 1;
	}
	
	$q = query("select * from plugin_barcodes_sample_types where project_id = $pid");
	// If project doesn't exist, send to Home page
//	if (mysql_num_rows($q) < 1) redirectHome();
	// Assign all redcap_projects table fields as variables and/or constants
	
	while ( $row = mysqli_fetch_assoc($q)) 
	{
	   // var_dump($row);
		$sample_array[$row['sample_type_id']] = $row;
		//$sample[$value['sample_type_id']]['name'] = $value['name'];	
		//$sample[$value['sample_type_id']]['multi'] = $value['n'];
	}
	
	//My Reports list
	if (REDCap::isLongitudinal()) 
	{
        $event_array = getEvents();
		print "<div style='max-width:700px;'><table width=100% cellpadding=3 cellspacing=0 style='border:1px solid #D0D0D0;font-family:Verdana,Arial;'>";	
		if ('asd' != "") {
			$i = 1;
			
			print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:right;width:20px;'>&nbsp;</td>
					<td class='notranslate' style='padding: 3px 0 3px 0;font-size:11px;font-weight:bold;'><b>Sample Types</b></td>";
				foreach( $sample_array as $sample_id => $sample ) {
					print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:center;width:70px;'>$sample[name]</td>";
					
				}
				print "</tr>";
			
			foreach ($event_array as $event_id => $event) {
		//	var_dump($this_array);
				$thisbg = ($i%2) == 0 ? '#FFFFFF' : '#EEEEEE';
				$this_menu_item = $this_array['name'];
				
				print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:right;width:30px;'>$i.)&nbsp;</td>
					<td class='notranslate' style='padding: 3px 2px 3px 0;font-size:11px;font-weight:bold;'>{$event['name']} <small>({$event['unique_name']})</?small></td>";
				foreach( $sample_array as $sample_id => $sample ) {
					print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:center;width:30px;'>";
					if($edit_sample_id['sample_type_id'] == $sample_id){
						print "<input type ='checkbox' name=event[".$event_id."] ";
						if($event_sample_array[$sample_id][$event_id]) {
							print " checked ";
							
						}
						print ">";
						
					}
					else {
						
						if($event_sample_array[$sample_id][$event_id]) {
							print "X";
							
						}
						else {
							print "&nbsp;";
						}
						
					}
					print "</td>";
				
				}
				print "</tr>";
				
				$i++;
			}
		}
		print "</table></div><br>";
	}

	
}



?>
