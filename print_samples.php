<?php

require_once dirname(__FILE__) . '/inc/config.php';
include APP_PATH_DOCROOT . 'ProjectGeneral/header.php';

print <<<EOF

            <p>Printing options:</p>
            Each user will likely have their own requirements for printing barcodes.  The following are some different ways to accomplish the task for a Zebra brand printer.
            <ol>
              <li>Purchase and run <a href="https://qz.io/">qztray</a> on client machines.
              <li>Create a CUPS raw printer on the local machine an print the raw EPL text using <i>lpr</i>.
            </ol>
            
          <br>
          <br>
          <b>Raw EPL code</b>
EOF;


if(isset($_GET['sample_id'])) {

	echo "<p class=barcodeData>" . getZebraSample($_GET['sample_id']) ."</p>";
	echo "<b>Raw ZPL code</b>";
	echo "<p class=barcodeData>" . getZebraSample($_GET['sample_id'], 'ZPL') ."</p>";

}
else if (isset($_GET['event_id'])){
	
	echo "<p class=barcodeData>" . getEventSamples($_GET['record'], $_GET['event_id']) ."</p>";

}
else {
	echo "No event or sample specified.";
}



include APP_PATH_DOCROOT . 'ProjectGeneral/footer.php';

function getEventSamples($record, $event_id, $format = 'EPL'){
	$r = mysqli_real_escape_string($record);
	$eid = mysqli_real_escape_string($event_id);

	$sql = "select to_base(sample_id) bc, sample_id,st.sample_type_id, st.name, s.event_id,  s.project_id, s.record
 from plugin_barcodes_samples s
join plugin_barcodes_sample_types st on s.sample_type_id = st.sample_type_id
join redcap_projects rp on rp.project_id = s.project_id 
where s.event_id= '{$eid}' and s.record = '{$r}'";

	$q = mquery($sql);

	while ($row = mysqli_fetch_assoc($q)) {
		$samples[] = $row;
	}

	$res = "";

	foreach ($samples as $sample) {
		$res .= getZebraSample($sample['sample_id'], $format);

	}

	return $res;

}

function getZebraSample($sample_id, $format='EPL'){
	$sid = mysql_real_escape_string($sample_id);

	$sql = "select to_base(sample_id) bc, sample_id,st.sample_type_id, st.name, s.event_id,  s.project_id, s.record
 from plugin_barcodes_samples s
join plugin_barcodes_sample_types st on s.sample_type_id = st.sample_type_id
join redcap_projects rp on rp.project_id = s.project_id 
where s.sample_id= '{$sid}'";
	$q = query($sql);

	$row = mysqli_fetch_assoc($q);
	
	$event_array = getEvents($row['project_id']);
	
	$barcode = str_pad($row['bc'], 4, '0', STR_PAD_LEFT);
	$project = REDCap::getProjectTitle();
	$event = $event_array[$row['event_id']]['name'];
	$type = $row['name'];
	$record = $row['record'];

	if($format == 'EPL'){


return <<<EOF
S4<br>
N<br>
q230<br>
D14<br>
B180,0,1,3,1,2,22,N,"{$barcode}"<br>
A155,27,1,2,1,1,N,"{$barcode}"<br>
A135,27,1,2,1,1,N,"{$project}"<br>
A135,177,1,2,1,1,N,"{$record}"<br>
A95,27,1,2,1,1,N,"{$event}"<br>
A75,27,1,3,1,1,N,"{$type}"<br>
P<br>
&nbsp;<br>
EOF;
	}
	else if($format =='ZPL'){
		return <<<EOF
^XA<br>
^PW230<br>
^FO120,30^BY1<br>
^B3R,N,22,N,N<br>
^FD{$barcode}^FS<br>
^FO98,30<br>
^ADR,18,10<br>
^FD{$barcode}^FS<br>
^FO75,30<br>
^ADR,18,10<br>
^FD{$project}^FS<br>
^FO80,195<br>
^ADR,18,10<br>
^FD{$record}^FS<br>
^FO50,30<br>
^ADR,18,10<br>
^FD{$event}^FS<br>
^FO29,30<br>
^ADR,18,10<br>
^FD{$type}^FS<br>
^XZ<br>
EOF;
	}


}

?>
