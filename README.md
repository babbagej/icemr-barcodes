# README #

This README goes over the steps are necessary to get some initial barcode features up and running.

### What does this code do? ###

This is not a complete plugin for printing barcodes.  Each institution will have a custom configuration and requirements.  The final result is the generation of ZPL and EPL that could be used in the local printing environment.

### How do I get set up the plugin? ###

* Place code in 'barcodes' within the plugin directory
* Modify /inc/config.php to include the project id of all projects that should have access to the plugin in the list of projects
* Open the URL <server>/plugins/barcodes/Resources/install.php, copy this code, and execute it on your REDCap database server.
* Create a bookmark to /plugins/barcodes/manage_sample_types.php that includes the project_id
* Create a bookmark to /plugins/barcodes/subject_samples.php that includes project_id and record info

### Usage ###

After doing the setup for the plugin, you need to set up the sample types for your particular project.  This is done though the manage sample types bookmark.  If a project is cross sectional, then you just create a list of names for the different samples you would want to identify for the project.  If the project is longitudinal, you first need to set up the events.   Once this is done, you can create the list of sample types.  Finally, you associate the sample type with events in the much the same way as you designate instruments for events, except it is done one sample type at a time by editing the sample type.

When viewing the Subject Samples page, any sample records that should exist given the sample profile, and do not currently exists will be generated before rendering the page.  Then the subject samples page will provide link generate the ZPL or EPL for each of the samples.  This is as a very simple method that generates printing code for one sample at a time.  It is anticipated that the code will be customized by groups that need to print batches of samples at once.  

### Who do I talk to? ###

* jon.babbage@gmail.com