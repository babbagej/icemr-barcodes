
CREATE TABLE `plugin_barcodes_samples` (
  `sample_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `record` varchar(255) NOT NULL,
  `sample_type_id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sample_id`),
  KEY `project_record` (`project_id`,`record`),
  KEY `project_record_event` (`project_id`,`record`, `event_id`),
  KEY `record` (`record`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `plugin_barcodes_sample_types` (
  `sample_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`sample_type_id`),
  KEY `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `plugin_barcodes_events_samples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_type_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `generate_number` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`sample_type_id`,`event_id`),
  KEY `sampletype` (`sample_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

