<?php

//require_once dirname(__FILE__) . '/../../config.php';
require_once dirname(__FILE__) . '/../../../../redcap_connect.php';



echo <<<EOF

USE `$db`;
DROP function IF EXISTS `to_dec`;
DROP function IF EXISTS `to_base`;
grant execute on $db.* to '$username'@$hostname;
DELIMITER $$
$$
create FUNCTION `to_dec`( base varchar(10) ) RETURNS int(11) DETERMINISTIC
BEGIN

declare x, result int;
declare hex_lookup  varchar(34);
set hex_lookup = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
set x = 0;
set result = 0;
set base = reverse(upper(base));

while( x<length(base)) do
	set result = result + ((locate(substr(base,x+1,1), hex_lookup) -1) * pow(34, x));

  set x = x+1;
end while;
 

#set result = locate(substr(base,0,1), hex_lookup);
return result;

END$$


CREATE FUNCTION `to_base` (number int(20))
RETURNS varchar(10) DETERMINISTIC
BEGIN



declare l_num int;
declare result varchar(10);
declare hex_lookup  varchar(34);
set hex_lookup = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
set result = '';

set l_num = number;

if l_num = 0 then
	return '0';
end if;

while( length(result) = 0 or l_num != 0) do
	set result = concat(substr(hex_lookup, (l_num%34) + 1, 1), result );
    set l_num = floor(l_num/34); 
end while;
return result;


END

$$

DELIMITER ;
EOF;

include "install.sql";
?>