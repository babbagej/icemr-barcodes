<?php

require_once dirname(__FILE__) . '/inc/config.php';
include APP_PATH_DOCROOT . 'ProjectGeneral/header.php';

include APP_PATH_DOCROOT . 'top.php';

renderPageTitle("<img src='".APP_PATH_IMAGES."qrcode.png'> Subject Samples");

print "<p>You may use this page view the samples associated with a subject and print labels for those samples.<br>";


confirmSamples(PROJECT_ID, $_GET['record']);


renderSubjectDetails(PROJECT_ID, $_GET['record']);


include APP_PATH_DOCROOT . 'ProjectGeneral/footer.php';


function renderSubjectDetails($pid, $record){

    $sql = "select to_base(sample_id) bc, sample_id,st.sample_type_id, st.name, s.event_id
 from plugin_barcodes_samples s
join plugin_barcodes_sample_types st on s.sample_type_id = st.sample_type_id
where s.project_id = {$pid} and record = '{$record}'
order by s.sample_type_id ";

    $q = query($sql);

	while ( $row = mysqli_fetch_assoc($q))
	{
        $sample_array[$row['event_id']][$row['sample_type_id']] = $row;
        $sample_types_for_subject[$row['sample_type_id']] = $row['name'];
	}
    
    $event_array = getEvents($pid);
	if (REDCap::isLongitudinal())
	{
        
		print "<div style='max-width:700px;'><table width=100% cellpadding=3 cellspacing=0 style='border:1px solid #D0D0D0;font-family:Verdana,Arial;'>";
			$i = 1;

			print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:right;width:20px;'>&nbsp;</td>
					<td class='notranslate' style='padding: 3px 0 3px 0;font-size:11px;font-weight:bold;'><b>Sample Types</b></td>";
				foreach( $sample_types_for_subject as $sample_type_id => $sample_type ) {
					print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:center;width:70px;'><a href = print_samples.php?record={$record}&sample_type_id={$sample_type_id}>$sample_type</a></td>";

				}
				print "</tr>";

			foreach ($event_array as $event_id => $event) {
				$thisbg = ($i%2) == 0 ? '#FFFFFF' : '#EEEEEE';
				$this_menu_item = $this_array['name'];

				print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:right;width:30px;'>$i.)&nbsp;</td>
					<td class='notranslate' style='padding: 3px 2px 3px 0;font-size:11px;font-weight:bold;'><a href = print_samples.php?pid={$pid}&record={$record}&event_id={$event_id}>{$event['name']}</a> <small>({$event['unique_name']})</?small></td>";
				foreach($sample_types_for_subject as $sample_type_id => $sample_type  ) {
					print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:center;width:30px;'>";

						if(isset($sample_array[$event_id][$sample_type_id])) {
							print "<a href=print_samples.php?pid={$pid}&record={$record}&sample_id={$sample_array[$event_id][$sample_type_id]['sample_id']}>";
							print $sample_array[$event_id][$sample_type_id]['bc']."</a>";

						}
						else {
							print "&nbsp;";
						}


					print "</td>";

				}
				print "</tr>";

				$i++;
			}
		print "</table></div><br>";
	}
	else {
        $event_id = $event_array[0];
        print "<div style='max-width:700px;'><table width=100% cellpadding=3 cellspacing=0 style='border:1px solid #D0D0D0;font-family:Verdana,Arial;'>";
			$i = 1;

			print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:right;width:20px;'>&nbsp;</td>
					<td class='notranslate' style='padding: 3px 0 3px 0;font-size:11px;font-weight:bold;' colspan=2><b>Sample Types</b></td>";
				
				print "</tr>";

			foreach ( $sample_types_for_subject as $sample_type_id => $sample_type ) {
				$thisbg = ($i%2) == 0 ? '#FFFFFF' : '#EEEEEE';
				$this_menu_item = $this_array['name'];

				print "<tr style='background-color: $thisbg;'>";
				print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:right;width:30px;'>$i.)&nbsp;</td>";
                print "<td style='padding: 3px 2px 3px 0;color:#808080;font-size:11px;text-align:left;width:300px;'><a href = print_samples.php?pid={$pid}&record={$record}&sample_type_id={$sample_type_id}>$sample_type</a></td>";
				
				print "<td style='padding: 3px 0 3px 0;color:#808080;font-size:11px;text-align:center;width:30px;'>";

                    if(isset($sample_array[$event_id][$sample_type_id])) {
				        print "<a href=print_samples.php?pid={$pid}&record={$record}&sample_id={$sample_array[$event_id][$sample_type_id]['sample_id']}>";
				        print $sample_array[$event_id][$sample_type_id]['bc']."</a>";

				    }
				    else {
                        print "&nbsp;";
				    }


					print "</td>";

				
				print "</tr>";

				$i++;
			}
		print "</table></div><br>";
	}

}
 


?>
